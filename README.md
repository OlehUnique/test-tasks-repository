# Odd/Even Linked Lists Function#



### Task ###

Implement a function that will get a pointer to a one-way-linked list head and return an array of two linked lists.
First of them should contain only odd elements from the input, while the second only even elements. Both returned lists should be in a reversed order.

### Function: ###

    func oddEvenLinkedListsReversed<T>(from head: Node<T>?) -> [LinkedList<T>]? {
        guard head != nil else { return nil }
        
        let oddElementsLinkedList = LinkedList<T>()
        let evenElementsLinkedList = LinkedList<T>()
        
        var odd = head, even = head?.next
        
        while odd != nil {
            
            if let oddValue = odd?.value {
                oddElementsLinkedList.insert(data: oddValue, at: 0)
            }
            
            if let evenValue = even?.value {
                evenElementsLinkedList.insert(data: evenValue, at: 0)
            }
            
            odd = even?.next
            even = odd?.next
        }
        
        return [oddElementsLinkedList, evenElementsLinkedList]
    }
