import UIKit
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

// ***************************************************************************************
//Implement a function that will get a pointer to a one-way-linked list head and return an array of two linked lists.
//First, of them, they should contain only odd elements from the input, while the second only even elements. Both returned lists should be in a reversed order
// ***************************************************************************************

public class Node<T> {
    var value: T
    var next: Node<T>?
    
    init(value: T) {
        self.value = value
    }
}

class LinkedList<T> {
    
    var head: Node<T>? // head is nil when list is empty
    
    public var isEmpty: Bool {
        return head == nil
    }
    
    public var first: Node<T>? {
        return head
    }
    
    public func append(value: T) {
        let newNode = Node(value: value)
        if var h = head {
            while h.next != nil {
                h = h.next!
            }
            h.next = newNode
        } else {
            head = newNode
        }
    }
        
    func insert(data : T, at position : Int) {
        let newNode = Node(value: data)
        
        if position == 0 {
            newNode.next = head
            head = newNode
        } else {
            var previous = head
            var current = head
            for _ in 0..<position {
                previous = current
                current = current?.next
            }
            
            newNode.next = previous?.next
            previous?.next = newNode
        }
    }
    
    func deleteNode(at position: Int)
    {
        if head == nil {
            return
        }
        var temp = head
        
        if position == 0 {
            head = temp?.next
            return
        }
        
        for _ in 0..<position - 1 where temp != nil {
            temp = temp?.next
        }
        
        if temp == nil || temp?.next == nil {
            return
        }
        
        let nextToNextNode = temp?.next?.next
        temp?.next = nextToNextNode
    }
    
    func printList() {
        var current: Node? = head
        //assign the next instance
        while current != nil {
            print("LL item is: \(current?.value as? Int ?? 0)")
            current = current?.next
        }
    }
        
    func printReverseRecursive(node: Node<T>?) {
        if node == nil {
            return
        }
        printReverseRecursive(node: node?.next)
        print("LL item is: \(node?.value as? Int ?? 0)")
    }
    
    func printReverse() {
        printReverseRecursive(node: first)
        
    }
}

func oddEvenLinkedListsReversed<T>(from head: Node<T>?) -> [LinkedList<T>]? {
    guard head != nil else { return nil }
    
    let oddElementsLinkedList = LinkedList<T>()
    let evenElementsLinkedList = LinkedList<T>()
    
    var odd = head, even = head?.next
    
    while odd != nil {
        
        if let oddValue = odd?.value {
            oddElementsLinkedList.insert(data: oddValue, at: 0)
        }
        
        if let evenValue = even?.value {
            evenElementsLinkedList.insert(data: evenValue, at: 0)
        }
        
        odd = even?.next
        even = odd?.next
    }
    
    return [oddElementsLinkedList, evenElementsLinkedList]
}

// Testing
let linkedList = LinkedList<Int>()
linkedList.append(value: 1)
linkedList.append(value: 2)
linkedList.append(value: 3)
linkedList.append(value: 4)
linkedList.append(value: 5)
linkedList.append(value: 6)
linkedList.append(value: 7)

let result = oddEvenLinkedListsReversed(from: linkedList.head)

print("Odd list:")
result?[0].printList()
print("Even list:")
result?[1].printList()
